@echo off
title ping-file %pingAddress%

set tab=   


echo [Enter] for defaults
echo.
echo Set Ping Address:
set "pingAddress="
set /P pingAddress=
if not defined pingAddress set "pingAddress=1.1.1.1"
cls

echo [Enter] for defaults
echo.
echo Set Save Filename:
set "fileName="
set /P fileName=
if not defined fileName set "fileName=ping-file-%~n0-%pingAddress%"
cls

echo [Enter] for defaults
echo.
echo Set Ping Size (Bytes):
set "pingSize="
set /P pingSize=
if not defined pingSize set "pingSize=32"
cls

echo [Enter] for defaults (Enter nothing to be endless)
echo.
echo Set Ping Count:
set "pingCount="
set /P pingCount=
cls

if not defined pingCount (goto :pingEndless) else (goto :pingIterated)

pause


:pingIterated
ping -t %pingAddress% -l %pingSize%|cmd /q /v /c "(pause&pause)>nul & for /l %%a in (1, 1, %pingCount%) do (set /p "data=" && echo(!date! %tab% !time! %tab% !data! )&ping -n 2 localhost>nul" >> %fileName%.txt
exit

:pingEndless
ping -t %pingAddress% -l %pingSize%|cmd /q /v /c "(pause&pause)>nul & for /l %%a in () do (set /p "data=" && echo(!date! %tab% !time! %tab% !data! )&ping -n 2 localhost>nul" >> %fileName%.txt
exit