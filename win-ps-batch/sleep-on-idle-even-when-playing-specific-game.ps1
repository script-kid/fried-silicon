# Define process name exactly... ignore the .exe
$specifiedProcessName = "Discord"

$specifiedIdleAfterSeconds = 5

# Remember the $ sign, switch these as you wish
[bool] $shouldKillProcess = $false
[bool] $shouldSleepWIndows = $true

# See:
# https://stackoverflow.com/questions/15845508/get-idle-time-of-machine
Add-Type @'
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace PInvoke.Win32 {

	public static class UserInput {

		[DllImport("user32.dll", SetLastError=false)]
		private static extern bool GetLastInputInfo(ref LASTINPUTINFO plii);

		[StructLayout(LayoutKind.Sequential)]
		private struct LASTINPUTINFO {
			public uint cbSize;
			public int dwTime;
		}

		public static DateTime LastInput {
			get {
				DateTime bootTime = DateTime.UtcNow.AddMilliseconds(-Environment.TickCount);
				DateTime lastInput = bootTime.AddMilliseconds(LastInputTicks);
				return lastInput;
			}
		}

		public static TimeSpan IdleTime {
			get {
				return DateTime.UtcNow.Subtract(LastInput);
			}
		}

		public static int LastInputTicks {
			get {
				LASTINPUTINFO lii = new LASTINPUTINFO();
				lii.cbSize = (uint)Marshal.SizeOf(typeof(LASTINPUTINFO));
				GetLastInputInfo(ref lii);
				return lii.dwTime;
			}
		}
	}
}
'@


Function killSpecifiedProcess {
	# try close
	$specifiedProcess.CloseMainWindow()

	Start-Sleep 5
	
	# force kill if not exited
	if (!$specifiedProcess.HasExited) {
	  $specifiedProcess | Stop-Process -Force
	}
}


Function sleepWindowsNow {
	# Sleep
	cmd /c "%windir%\System32\rundll32.exe powrprof.dll,SetSuspendState 0,1,0"

	# # 1. Define the power state you wish to set, from the
	# #	 System.Windows.Forms.PowerState enumeration.
	# $PowerState = [System.Windows.Forms.PowerState]::Suspend;

	# # 2. Choose whether or not to force the power state
	# $Force = $false;

	# # 3. Choose whether or not to disable wake capabilities
	# $DisableWake = $false;

	# # Set the power state
	# [System.Windows.Forms.Application]::SetSuspendState($PowerState, $Force, $DisableWake);
}





# for ( $i = 0; $i -lt 10; $i++ ) {
while (1) {
	# Write-Host ("Last input " + [PInvoke.Win32.UserInput]::LastInput)

	# Get IdleTime in hh:mm:ss,ff
	# This continues ticking even if watching media or with a game running in the background
	# i.e. idle time goes up even if game is open
	$idleTime = [PInvoke.Win32.UserInput]::IdleTime

	Write-Host ("Idle for " + $idleTime.Seconds + " Seconds")

	# Check if PC has been idle for more than the specified amount
	if ($idleTime.Seconds -gt $specifiedIdleAfterSeconds) {

	  # get process by specified name in top of file
	  $specifiedProcess = Get-Process $specifiedProcessName -ErrorAction SilentlyContinue

	  # If process detected
	  if ($specifiedProcess) {

		 if ($shouldKillProcess -eq $true) {
			killSpecifiedProcess
		 }

		 if ($shouldSleepWIndows -eq $true) {
			sleepWindowsNow
		 }

	  }
	}

	# Loop every second
	Start-Sleep -Seconds (1)
}