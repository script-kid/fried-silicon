echo off


REM ----------------------------------------------------------------------------
REM This file requires ffmpeg and youtube-dl, or another flavour of youtube-dl.
REM I'm now using: https://github.com/yt-dlp/yt-dlp
REM A cookies file is also expected to be in cookies-youtube-com.txt
REM Usage:
REM invoke this file with params:
REM - bandwidth (number only, in megabits)
REM - "yes" to use playlist, anything else to not
REM - output path, avoid spaces
REM - YouTube link, use quotes
REM ----------------------------------------------------------------------------


REM ----------------------------------------------------------------------------
REM Take params
REM ----------------------------------------------------------------------------


set argBandwidth=%1
set argPlaylist=%2
set argOutPath=%3
set argUrl=%4

REM Default archive if no param specified
if "%5" == "" (
	set argDownloadArchive=_downloaded.txt
) else (
	set argDownloadArchive=%5
)

if %argBandwidth% == "" (
	echo "argBandwidth blank"
	goto :eof
)
if %argPlaylist% == "" (
	echo "argPlaylist blank"
	goto :eof
)
if %argOutPath% == "" (
	echo "argOutPath blank"
	goto :eof
)
if %argUrl% == "" (
	echo "argUrl blank"
	goto :eof
)


if "%argPlaylist%" == "yes" (
	goto :playlistAndBandwidth
) else (
	goto :noPlaylistAndbandwidth
)

goto :eof



@REM the %% allows the var to be set within the new instance or whatever, % is for vars from this file here

:playlistAndBandwidth
rem cmd /k "youtube-dl -f bestvideo+bestaudio --merge-output-format mkv --ffmpeg-location ffmpeg/bin -o "/Download/%argOutPath%/%%(title)s.%%(ext)s" --embed-thumbnail -i --download-archive %argDownloadArchive% --rate-limit %argBandwidth%m --yes-playlist %argUrl% --cookies cookies-youtube-com.txt"
cmd /k "yt-dlp -f bestvideo+bestaudio --merge-output-format mkv --ffmpeg-location ffmpeg/bin -o "/Download/%argOutPath%/%%(title)s.%%(ext)s" --embed-thumbnail -i --download-archive %argDownloadArchive% --rate-limit %argBandwidth%m --yes-playlist %argUrl% --cookies cookies-youtube-com.txt"
goto :eof

:noPlaylistAndBandwidth
rem cmd /k "youtube-dl -f bestvideo+bestaudio --merge-output-format mkv --ffmpeg-location ffmpeg/bin -o "/Download/%argOutPath%/%%(title)s.%%(ext)s" --embed-thumbnail -i --download-archive %argDownloadArchive% --rate-limit %argBandwidth%m %argUrl% --cookies cookies-youtube-com.txt"
cmd /k "yt-dlp -f bestvideo+bestaudio --merge-output-format mkv --ffmpeg-location ffmpeg/bin -o "/Download/%argOutPath%/%%(title)s.%%(ext)s" --embed-thumbnail -i --download-archive %argDownloadArchive% --rate-limit %argBandwidth%m %argUrl% --cookies cookies-youtube-com.txt"
goto :eof
