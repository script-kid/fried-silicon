@echo off


REM Set the IP you want to ping here
SET pingAddress=1.1.1.1
REM Set the ping size you want, in bytes
SET pingSize=65500
REM Get the filename
SET fileName="%~n0"

title ping-console %pingAddress%

ping -t %pingAddress% -l %pingSize%|cmd /q /v /c "(pause&pause)>nul & for /l %%a in () do (set /p "data=" && echo(!time! !data!)&ping -n 2 localhost>nul"



pause