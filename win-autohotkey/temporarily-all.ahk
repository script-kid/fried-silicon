; ; Tab into VSCode
; ^+v::
; {
; 	SetTitleMatchMode, 2
; 	WinActivate, - Visual Studio Code
; }
; return

; ; Tab into Discord
; ^+d::
; {
; 	SetTitleMatchMode, 2
; 	WinActivate, - Discord
; }
; return

; Reference Harvard copy
^+r::
{

	; Because MS Word is MS Word, this script needs a blank new line below the current working line, otherwise "End" key will go to the page brake

	; Delay bullshit
	SetKeyDelay 30,50

	; Prefix text that pastes first
	prefixText := "Available at: "

	; Date prefix
	accessedPrefix := " (Accessed: "

	; Window title can contain WinTitle anywhere inside it to be a match
	SetTitleMatchMode, 2
	; As it always has the "- Mozilla Firefox" part in it
	WinActivate, - Mozilla Firefox
	; CTRL L
	Send, ^l
	; CTRL C
	Send, ^c


	; Store the URL
	pageUrl := Clipboard


	; Create the HTML request
	WebObj := ComObjCreate("WinHttp.WinHttpRequest.5.1")
	; Set how to make the request and where
	WebObj.Open("GET", pageUrl)
	; Makes the request
	WebObj.Send()
	; Gets the response text (entire HTML file, puts into string)
	HtmlText := WebObj.ResponseText
	; Gets whatever the fuck is in the title tags, idk - magic. Result is in titleText
	; RegExMatch(HtmlText, "is)<title>\K(.*?)</title>", titleText)
	RegExMatch(HtmlText, "<title>\K.*?(?=</title>)", titleText)


	; Alt tab back into wherever you were before
	Send, {AltDown}{Tab}{AltUp}

	; Paste HTML Title text
	Clipboard := titleText
	Send, ^v

	; Get length of pasted string
	titleTextLength:=StrLen(Clipboard)
	; Highlight pasted
	SendInput {shift down}{Left %titleTextLength%}{shift up}

	; Italicize selection
	Send, ^i

	; Go to the end of the line
	Send, {End}

	; Stop italics
	Send, ^i

	; Full stop at end with space
	Send, .{Space}

	; Paste prefix text "Available at: "
	Clipboard := prefixText
	Send, ^v

	; Paste page URL
	Clipboard := pageUrl
	Send, ^v

	; Paste urly bracket and prefix text shit
	Clipboard := accessedPrefix
	Send, ^v

	; Paste current date in format that matches: "28 March 2020"
	Clipboard := % A_DD " "A_MMMM " " A_YYYY
	Send, ^v
	; Sends close curly bracket and full stop
	SendRaw, ).
	; New line
	Send, {Enter}
}
return