; ------------------------------------------------------------------------------
; This script is very finnicky
; as the tabbing in explorer.exe
; is utterly crap
; so when the script enters CTRL + L to enter the directory
; it often doesn't properly tab in a way to allow
; CTRL + Enter to accept the save dir
; 
; It's also hard coded for *my* folders with *my* context menu as it goes down
; as many times as it needs to for me.
; ------------------------------------------------------------------------------

SetKeyDelay, [ 30, 50]
; Ctrl Shift V
^+v::
{
	Sleep, 100
	Click right
}
return

^+b::
{
	; --------------------------------------------------------------------------
	; Windows explorer
	; --------------------------------------------------------------------------

	; Go down to 7Zip after RMB
	Loop 11 {
		Send {Down}
	}

	; Select submenu
	Send {Right}

	; Select add to archive
	Send {Enter}

	Sleep, 500

		
	; --------------------------------------------------------------------------
	; 7Zip
	; --------------------------------------------------------------------------

	; Select output path
	Send {Tab}
	Send {Space}

	Sleep, 1000

	; --------------------------------------------------------------------------
	; Windows Explorer Save Menu
	; --------------------------------------------------------------------------

	; Select Explorer path, CTRL L
	Send ^l

	; Type path
	SendRaw Z:\XDrive

	; Go to path
	Sleep, 200
	Send {Enter}

	Sleep, 200

	; Tab out of the stupid selection? of the location
	Send {Tab}
	Send {Tab}
	Send {Tab}
	Send {Tab}
	Send {Tab}

	; Okay in explorer
	Send {CtrlDown}{Enter}{CtrlUp}
	Sleep, 200

	; --------------------------------------------------------------------------
	; 7Zip
	; --------------------------------------------------------------------------

	; Tab to password
	Loop 15 {
		Send {Tab}
	}

	; Send password
	SendRaw ZipPasswordHere

	; Send {Enter}
}
return
