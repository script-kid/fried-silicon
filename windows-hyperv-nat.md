These are notes regarding Windows 10 NAT setup I was experimenting with when using HyperV.

This is poorly formatted and is _not_ a guide, just a "journal".


```
// https://www.youtube.com/watch?v=PYamsYQSmFY

New-VMSwitch -SwitchName ArchSwitch -SwitchType Internal

Get-NetAdapter

// Set InterfaceIndex to the ifIndex of ArchSwitch from Get-NetAdapter
// New-NetIPADdress -IPAddress 10.0.1.1 -PrefixLength 24 -InterfaceIndex 27
New-NetIPADdress -IPAddress 10.0.1.1 -PrefixLength 24 -InterfaceIndex 10
// New-NetIPADdress -IPAddress 192.168.0.169 -PrefixLength 24 -InterfaceIndex 27
// New-NetIPADdress -IPAddress 192.168.0.1 -PrefixLength 24 -InterfaceIndex 27

// New-NetIPAddress -IPAddress 192.168.0.1 -PrefixLength 24 -InterfaceAlias "vEthernet (ArchSwitch)"

// New-NetNat -Name ArchNAT -InternalIPInterfaceAddressPrefix 192.168.0.0/24
// New-NetNat -Name ArchNAT -InternalIPInterfaceAddressPrefix 192.168.0.120/24
New-NetNat -Name ArchNAT -InternalIPInterfaceAddressPrefix 192.168.0.0/24


// Connect-VMNetworkAdapter -VMName archlol -SwitchName "ArchSwitch"


## SSH and host and guest network
New-VMSwitch -SwitchName ArchSwitch -SwitchType Internal
New-NetIPADdress -IPAddress 10.0.1.1 -PrefixLength 24 -InterfaceIndex 27
New-NetNat -Name ArchNAT -InternalIPInterfaceAddressPrefix 192.168.0.0/24

Then set Virtual Switch to "External network" with "Allow management operating system to share this network adapter" and make sure IPv4 is enabled on the normal NAT with 192.168.0.113 IP.

Arch VM just needs to assign its static IP (192.168.0.120) and SSH works to that IP.

For some reason my host was effectively on DHCP even though my LAN was still 192.168.0.113.

I had to set ArchSwitch to use 192.168.0.113 and my LAN to 192.168.0.123 (VM is on .120, so bit less ambiguous)

Then any ports can be exposed with
Add-NetNatStaticMapping  -NatName NATNetwork  -Protocol TCP  -ExternalIPAddress 0.0.0.0/24  -ExternalPort 1443  -InternalIPAddress 192.168.0.120  -InternalPort 443


## SSH Working

New-VMSwitch -SwitchName ArchSwitch -SwitchType Internal
New-NetIPADdress -IPAddress 192.168.0.1 -PrefixLength 24 -InterfaceIndex 22
New-NetNat -Name ArchNAT -InternalIPInterfaceAddressPrefix 192.168.0.0/24


## Host and Guest network working, no SSH
Following https://mytechiethoughts.com/windows/easy-internet-access-for-hyper-v-guests-using-nat/

and setting Hyper-V switch to external network and re-enabling IPv4 on LAN


## SSH Working and VM internet, host no internet
New-VMSwitch -SwitchName ArchSwitch -SwitchType Internal
New-NetIPADdress -IPAddress 10.0.1.1 -PrefixLength 24 -InterfaceIndex 27
New-NetIPADdress -IPAddress 10.0.1.1 -PrefixLength 24 -InterfaceIndex 27
New-NetNat -Name ArchNAT -InternalIPInterfaceAddressPrefix 192.168.0.0/24

And setting the network as an external network in Hyper-V

it seems either way, the "Allow management operating system to share this network adapter" is
what makes the difference between host/guest network and guest+SSH

Setting the IPv4 to .113 and .114 or .120 or whatever makes it all work as I want, except for "DUP!" packets

```