// -----------------------------------------------------------------------------
// Method 1 that Luke found
// -----------------------------------------------------------------------------

// ID = bigCookie
function clickId(id) {
	// Gets the "bigCookie" element
	var element = document.getElementById(id);
	// If it's defined... aka it exists
	if (element !== undefined) {
		// run doEvent with the element we just grabbed, and pass "click" string to it for some dumb reason?
		doEvent(element, "click");
	}
	// run this function again after 25 milliseconds... this is a way to make a loop,
	// as opposed to a setInterval which just repeats inherently
	window.setTimeout(clickId, 25, id);
}

// Function passed with a HTML Element and "click" string
function doEvent(element, type) {
	// just create an event thingy... should be prefixed with 'const' or 'var' but it works
	trigger = document.createEvent('HTMLEvents');
	// do the thingy, whatever initEvent actually does, and give it the "click" string
	trigger.initEvent(type, true, true);
	// dispatch the event we defined... whatever that dies
	element.dispatchEvent(trigger);
}

// FIRST: runs clickId once after 25ms and provides it with argument "bigCookie"
window.setTimeout(clickId, 25, "bigCookie");
window.setTimeout(clickId, 25, "goldenCookie");


// -----------------------------------------------------------------------------
// My rewrite
// -----------------------------------------------------------------------------

const thingyCookie = document.getElementById('bigCookie');
const thingyGolden = document.getElementById('goldenCookie');

const clickerClicker = {};

clickerClicker.startClicker = () => {
	clickerClicker.timer = setInterval(() => {
		const trigCookie = document.createEvent('HTMLEvents');
		const trigGolden = document.createEvent('HTMLEvents');
		trigCookie.initEvent('click', true, true);
		trigGolden.initEvent('click', true, true);
		thingyCookie.dispatchEvent(trigCookie);
		thingyGolden.dispatchEvent(trigGolden);
	}, 1);
}

clickerClicker.stopClicker = () => {
	clearInterval(clickerClicker.timer);
};

// -----------------------------------------------------------------------------
// yeet
// -----------------------------------------------------------------------------

const thingyCookie = document.getElementById('bigCookie');
const thingyGolden = document.getElementById('goldenCookie');

const airiAdd = () => {
	const trigCookie = document.createEvent('HTMLEvents');
	const trigGolden = document.createEvent('HTMLEvents');
	trigCookie.initEvent('click', true, true);
	trigGolden.initEvent('click', true, true);
	thingyCookie.dispatchEvent(trigCookie);
	thingyGolden.dispatchEvent(trigGolden);
}

setInterval(airiAdd, 1);