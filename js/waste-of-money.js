
const subs = {};

// Ungrouped
subs["Amazon Prime"] = "7.99GBP";
subs["Discord"] = "9.99GBP";

// Dev
subs["VPS"] = "10GBP";
subs["GitHub pro"] = "4USD";
subs["GitLab Premium"] = "19USD";

// Productivity
subs["Wikipedia"] = "2GBP";
subs["Google Drive"] = "8.28GBP";
subs["Office 365"] = "7.99GBP";
subs["Adobe CC"] = "40GBP";

// Fake-ass productivity
subs["Grammarly"] = "12USD";
subs["Trello Premium"] = "10USD";

// Content video
subs["Netflix"] = "9.99GBP";
subs["CrunchyRoll"] = "7.99GBP";
subs["YouTube Premium"] = "11.99GBP";
subs["Disney Plus"] = "7.99GBP";
subs["Twitch sub1"] = "3.99GBP";
subs["Twitch sub2"] = "3.99GBP";

// Content audio
subs["Audible"] = "7.99GBP";
subs["Spotify"] = "9.99GBP";
subs["Tidal"] = "19.99GBP";

// Content images
subs["Pixiv Premium"] = "3.58GBP"; // 550yen

// Sus
subs["NordVPN"] = "8.84GBP";


const totals = {};
totals["GBP"] = 0;
totals["USD"] = 0;


for (const key in subs) {
	if (Object.hasOwnProperty.call(subs, key)) {
		const element = subs[key];

		// If string starts with number, such as "4USD",
		// this will still get the int
		const rawAmount = parseInt(element);
		// console.log(rawAmount);

		// Strip all numbers to get the currency type
		const currencyType = element.replace(/[^a-zA-Z]/g, "");
		// console.log(currencyType);

		totals[currencyType.toString()] += rawAmount;
	}
}

console.log(totals);
