if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

$instances = Get-Process

foreach ($i in $instances) {
	if ($i.ProcessName -ne "obs64") {
		# Everything on first 6 threads
		$i.ProcessorAffinity = 15
	}

	elseif ($i.ProcessName -eq "obs64") {
		# obs on last 12 threads
		$i.ProcessorAffinity = 65520
	}
}