if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

$instances = Get-Process

foreach ($i in $instances) {
	if ($i.ProcessName -ne "obs64") {
		# Everything on first 8 threads
		$i.ProcessorAffinity = 255
	}

	elseif ($i.ProcessName -eq "obs64") {
		# obs on last 8 threads
		$i.ProcessorAffinity = 65280
	}
}