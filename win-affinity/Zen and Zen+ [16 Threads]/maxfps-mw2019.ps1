if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

$instances = Get-Process
# Set all processes but ModernWarfare to first 4 threads; set MW to last 12
foreach ($i in $instances) {
    if ($i.ProcessName -eq "ModernWarfare") {
        $i.ProcessorAffinity = 65520
    }
    elseif ($i.ProcessName -ne "ModernWarfare") {
        $i.ProcessorAffinity = 15
    }
}