if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }



$instances = Get-Process
foreach ($i in $instances) {
    if ($i.ProcessName -eq "ModernWarfare") {
        $i.ProcessorAffinity = 4095
    }
    elseif ($i.ProcessName -eq "obs64") {
        $i.ProcessorAffinity = 4294963200
    }
    else {
        $i.ProcessorAffinity = 4294967295
    }
}
pause