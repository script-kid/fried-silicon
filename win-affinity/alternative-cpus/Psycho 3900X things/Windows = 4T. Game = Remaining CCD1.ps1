
$instances = Get-Process
ForEach ($i in $instances) { $i.ProcessorAffinity=15}
# Makes all processes stick to the first 4 threads


ForEach($PROCESS in GET-PROCESS "bfv") { $PROCESS.ProcessorAffinity=65520}
# Run BFV within the first CCD, except for the first 4 threads
