if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }

$instances = Get-Process
foreach ($i in $instances) { $i.ProcessorAffinity=63 }

ForEach($PROCESS in GET-PROCESS "firefox") { $PROCESS.ProcessorAffinity=4032}
