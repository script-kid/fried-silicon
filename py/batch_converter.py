from multiprocessing.pool import ThreadPool
import subprocess
import argparse
import glob
import os
import time

# Taken from https://github.com/TigerhawkT3/small_scripts/blob/master/batch_converter.py
# and simplified. Less options, no PowerShell, no massive one-liners.

OUTPUT_FOLDER_NAME="jpg-to-jxl"

parser = argparse.ArgumentParser(prog='Batch converter',
    description='Parallel cjxl command',
    formatter_class=argparse.RawTextHelpFormatter,
    epilog='''see source''')
parser.add_argument('directory', help='Source directory containing your files.') # C:\images
# parser.add_argument('command', help='The conversion program to run, including options.') # "cjxl -j 0 -d 1"
parser.add_argument('-w', '--workers', type=int, default=4, help='Number of simultaneous CPU workers.') # 4
parser.add_argument('-s', '--source-extension',
    help='File type extention to convert (no leading period).') # -s jpg
args = parser.parse_args()

def do_convert(path_in, path_out):
    path_in_short = os.path.basename(path_in)
    path_out_short = os.path.basename(path_out)

    command = (
            "cjxl --lossless_jpeg=1 --quality=100 --effort=10 "
            + path_in_short
            + " "
            + path_out_short
        )
    print(" ----- executing: " + command)
    subprocess.run(
        ["cjxl", "--lossless_jpeg=1", "--quality=100", "--effort=10", path_in, path_out],
        check=False
    )

def main():
    tp = ThreadPool(args.workers)

    print("Working directory: " + args.directory)
    print("Output directory: " + args.directory + "/" + OUTPUT_FOLDER_NAME)

    files = []
    output_folder = args.directory + "/" + OUTPUT_FOLDER_NAME

    # Hard-coded output folder
    if not os.path.exists(output_folder):
        print("creating folder " + output_folder)
        os.makedirs(output_folder)

    for filename_with_path, ext in map(os.path.splitext, glob.iglob(args.directory + "/*." + args.source_extension, recursive=True)):
        ext_stripped = ext.lstrip(".")
        # basename gets filename without path
        files.append([os.path.basename(filename_with_path), ext_stripped])

    # New array of not-already-converted files
    files_to_convert = []
    for idx, (filename, ext) in enumerate(files, start=1):
        path_out = output_folder + "/" + filename + ".jxl"
        if not os.path.exists(path_out):
            files_to_convert.append([filename, ext])

    print("Total files count: ", len(files))
    print("Files to convert count: ", len(files_to_convert))
    print("")
    print("Files to convert list:")
    for idx, (filename, ext) in enumerate(files_to_convert, start=1):
        print(filename + "." + ext)
    print("")
    print("")
    print("waiting 3s... Ctrl+C to cancel")
    time.sleep(3)

    for idx, (filename, ext) in enumerate(files_to_convert, start=1):
        path_in = args.directory + "/" + filename + f'.{ext}'
        path_out = output_folder + "/" + filename + ".jxl"
        tp.apply_async(do_convert, (path_in, path_out))

    tp.close()
    tp.join()

main()
