
# You can include link file into ~/.bashrc via `source script.sh`
# and uncomment the below or define them in ~/.bashrc
# curl_helper_api_dev="https://api.domain"
# curl_helper_api_auth="https://api.domain/auth"
# curl_helper_api_key="KeyGoesHere"

# OPT: Function for curl helper "curl_helper" below
# Requests JWT from API with interactive credential prompt
# $1 = $api_key
# echoing to >&2 allows to print to console,
# while the normal echo is used for the return value.
# IMPORTANT: Echoing anything but the desired return value = a very bad time
function curl_helper_jwt {
	echo "--------------------" >&2
	echo "Fetch new JWT:" >&2
	read -rp "  - Enter email " curl2_email
	read -rp "  - Enter password " curl2_password
	curl2_response=$(eval "curl -s -X POST '$curl_helper_api_auth?key=$1' -H 'Content-Type: application/json' --data-raw '{\"type\":\"web\",\"email\":\"$curl2_email\",\"password\":\"$curl2_password\"}'")
	curl_jwt_token=$(echo "$curl2_response" | jq -r ".token")
	if [[ -z "$curl_jwt_token" || "$curl_jwt_token" = "null" ]] ; then
		echo >&2
		echo "Failed to authenticate." >&2
		# echo is return value, return is used for control flow
		echo "NotFound"
		return
	fi
	echo >&2
	echo "Your JWT:" >&2
	echo "  - $curl_jwt_token" >&2
	echo "--------------------" >&2
	# return value
	echo "$curl_jwt_token"
	return
}

# OPT: Function for curl helper "curl_helper" below
# Makes the actual request with provided options
# $1 = $curl_method
# $2 = $curl_uri
# $3 = $curl_json
# $4 = $curl_jwt_token
# echoing to >&2 allows to print to console,
# while the normal echo is used for the return value.
# IMPORTANT: Echoing anything but the desired return value = a very bad time
function curl_helper_attempt {
	curl_response=""
	if [ "$4" = "skip" ] ; then
		curl_response=$(eval "curl -s -i -X $1 '$2' $3")
	else
		curl_response=$(eval "curl -s -i -X $1 '$2' $3 -H 'Authorization: Bearer $4'")
	fi
	echo "Curl response:" >&2
	echo "--------------------" >&2
	echo "$curl_response" >&2
	echo "--------------------" >&2
	echo "$curl_response"
}

# curl helper for making requests to an API with automatic JWT token requesting
# You can copy this file into ~/.bashrc and uncomment the below function declaration
# to be able to call it anywhere.
function curl_helper {
	echo
	nums='^[0-9]+$'
	method_options=(
		"GET"
		"POST"
		"PUT"
		"DELETE"
	)
	api_base="http://localhost:3000/"
	# Hardcoded but a slight refactor could expose this as a param
	# and as something that's set based on prompted JWT login credentials
	api_key="$curl_helper_api_key"
	# ux_ values are used for interface stuff
	ux_json=""
	ux_query_params=""
	ux_uri=""
	# curl_ values are used for the command
	curl_method=""
	curl_endpoint=""
	curl_json=""
	curl_query_params=""
	curl_uri=""
	curl_jwt_token=""
	curl_response=""
	# These are defined/concatenated like this for printf to display properly
	info_pos1="  - \"dev\" to target our api \n"
	info_pos1="${info_pos1}  - \"-\" to target default localhost api \n"
	info_pos1="${info_pos1}  - <number> to change localhost port \n"
	info_pos2="  - REST method \n"
	info_pos2="${info_pos2}  - options: GET, POST, PUT, DELETE \n"
	info_pos3="  - set this to the endpoint URI that comes after the domain \n"
	info_pos3="${info_pos3}  - do not provide the API key here \n"
	info_pos4="  - JSON, set to \"-\" to skip \n"
	# Backslashes are for output formatting. This is how you should set the param: '{"field": "val"}'
	info_pos4="${info_pos4}  - encapsulate the JSON like this: '{\"field\": \"val\"}' \n"
	info_pos5="  - query param added after the built-in \"?key=\" \n"
	info_pos5="${info_pos5}  - encapsulate the query param like this: '&name=foo&test=bar' \n"
	info_pos5="${info_pos5}  - write query params like normal but start off with an ampersand \n"
	info_pos6="  - Bearer JWT input \n"
	# "./", "/"", or "~/"
	info_pos6="${info_pos6}  - this can be a filepath if it starts with \"./\", \"/\", or \"~/\" \n"
	info_pos6="${info_pos6}  - set to \"-\" or omit to be prompted for credentials to request a JWT \n"
	info_pos6="${info_pos6}  - the interactive prompt only authorises admins; player login is a TBD \n"
	info_pos6="${info_pos6}  - set to \"skip\" to avoid sending Authorization completely \n"
	if [[ -z "$1" || "$1" = "-h" || "$1" = "--help" || "$1" = "help" || "$1" = "h" ]] ; then
		echo "Usage (positional parameters):"
		echo "pos 1 (\$1):"
		printf '%b' "$info_pos1"
		echo "pos 2 (\$2):"
		printf '%b' "$info_pos2"
		echo "pos 3 (\$3):"
		printf '%b' "$info_pos3"
		echo "pos 4 (\$4):"
		printf '%b' "$info_pos4"
		echo "pos 5 (\$5):"
		printf '%b' "$info_pos5"
		echo "pos 6 (\$6):"
		printf '%b' "$info_pos6"
		echo
		echo "Example commands:"
		echo "  - curl_helper dev get player/ '{\"field\": \"val\"}' '&name=foo&v=bar' jwtTokenHere"
		echo "  - curl_helper - get player/ - - skip"
		return
	fi
	if [ "$1" = "dev" ]; then
		api_base="$curl_helper_api_dev"
	# If first arg is a number, use it as the port for localhost
	elif [[ "$1" =~ $nums ]] ; then
		api_base="${api_base/3000/"$1"}"
	elif [ ! "$1" = "-" ] ; then
		echo "--------------------"
		echo "\$1 Error: pos 1 param not set properly"
		printf '%b' "$info_pos1 \n"
		echo "Your value was: $1"
		echo "--------------------"
		return
	fi
	if [ -z "$2" ] ; then
		echo "--------------------"
		echo "\$2 Error: pos 2 param not set"
		printf '%b' "$info_pos2 \n"
		echo "Your value was: $2"
		echo "--------------------"
		return
	else
		for method in "${method_options[@]}"; do
			# The ^^ converts to user input uppercase
			if [[ "${2^^}" = "$method" ]] ; then
				curl_method="${2^^}"
				break
			fi
		done
		if [ -z "$curl_method" ] ; then
			echo "--------------------"
			echo "\$2 Error: pos 2 param not set properly"
			printf '%b' "$info_pos2 \n"
			echo "Your value was: $2"
			echo "--------------------"
			return
		fi
	fi
	if [ -z "$3" ] ; then
		echo "--------------------"
		echo "\$3 Error: pos 3 param not set"
		printf '%b' "$info_pos3 \n"
		echo "Your value was: $3"
		echo "--------------------"
		return
	else
		# Strip leading /
		if [[ ${3:0:1} = "/" ]] ; then
			curl_endpoint=${3:1}
		else
			curl_endpoint=$3
		fi
	fi
	# Set header and JSON data if provided and not "-"
	if [[ ! -z "$4" && "$4" != "-" ]] ; then
		ux_json="$4"
		curl_json="-H 'Content-Type: application/json' --data-raw '$4'"
	fi
	if [[ ! -z "$5" && "$5" != "-" ]] ; then
		# Split array by &
		# See: https://stackoverflow.com/a/5257398/13310905
		ux_query_params=(${5//&/ })
		curl_query_params="$5"
	fi
	ux_uri=$api_base$curl_endpoint
	echo "Request details:"
	echo "--------------------"
	echo "$method $ux_uri"
	echo "Key: $api_key"
	if [ -z "$ux_json" ] ; then
		echo "No request body"
	else
		echo "Request body JSON:"
		echo "  - $ux_json"
	fi
	if [ -z "$ux_query_params" ]; then
		echo "No query params"
	else
		echo "Query params:"
		for query in "${ux_query_params[@]}"; do
			echo "  - $query"
		done
	fi
	echo "--------------------"
	echo
	# Set JWT var from what's given, or read if a filepath is given.
	if [[ ! -z "$6" && "$6" != "-" ]] ; then
		if [[ ${6:0:2} = "./" || ${6:0:1} = "/" || ${6:0:2} = "~/" ]] ; then
			curl_jwt_token=$(eval "cat $6")
		else
			curl_jwt_token="$6"
		fi
	else
		# No token provided so fetch first
		echo
		curl_jwt_token=$(curl_helper_jwt "$api_key")
		if [ "$curl_jwt_token" = "NotFound" ] ; then
			return
		fi
		echo
	fi
	# echo "--------------------"
	# echo
	curl_uri="$api_base$curl_endpoint?key=$api_key$curl_query_params"
	curl_response=$(curl_helper_attempt "$curl_method" "$curl_uri" "$curl_json" "$curl_jwt_token")
	# Just look for the substring anywhere because the response isn't just JSON;
	# it contains a bunch of metadata
	# jq -r '.error'
	if [[ "$curl_response" = *"Your session has expired"* && "$curl_jwt_token" != "skip" ]] ; then
		echo
		curl_jwt_token=$(curl_helper_jwt "$api_key")
		if [ "$curl_jwt_token" = "NotFound" ] ; then
			return
		fi
		curl_response=$(curl_helper_attempt "$curl_method" "$curl_uri" "$curl_json" "$curl_jwt_token")
	fi
}
