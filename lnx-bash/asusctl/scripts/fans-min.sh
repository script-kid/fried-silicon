#!/bin/bash

# Matches Windows Battery Slow Cool

prcnt() {
    echo $((($1*10)*255/1000))
}

# NOTE: first two temps were previously set to 10% fan
main_command="-D 30c:$(prcnt "5"),40c:$(prcnt "5"),50c:$(prcnt "10"),60c:$(prcnt "25"),70c:$(prcnt "45"),80c:$(prcnt "70"),90c:$(prcnt "100"),100c:$(prcnt "100") -e true -f"

asusctl fan-curve -m performance $main_command gpu
asusctl fan-curve -m performance $main_command cpu

asusctl fan-curve -m balanced $main_command gpu
asusctl fan-curve -m balanced $main_command cpu

asusctl fan-curve -m quiet $main_command gpu
asusctl fan-curve -m quiet $main_command cpu

