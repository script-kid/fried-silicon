#!/bin/bash

# Matches fans-vslow with a floor of 30%

prcnt() {
    echo $((($1*10)*255/1000))
}

main_command="-D 30c:$(prcnt "30"),40c:$(prcnt "30"),50c:$(prcnt "30"),60c:$(prcnt "30"),70c:$(prcnt "50"),80c:$(prcnt "100"),90c:$(prcnt "100"),100c:$(prcnt "100") -e true -f"

asusctl fan-curve -m performance $main_command gpu
asusctl fan-curve -m performance $main_command cpu

asusctl fan-curve -m balanced $main_command gpu
asusctl fan-curve -m balanced $main_command cpu

asusctl fan-curve -m quiet $main_command gpu
asusctl fan-curve -m quiet $main_command cpu
