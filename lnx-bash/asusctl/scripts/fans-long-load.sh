#!/bin/bash

# Matches Windows AC Long Load

prcnt() {
    echo $((($1*10)*255/1000))
}

main_command="-D 30c:$(prcnt "50"),40c:$(prcnt "50"),50c:$(prcnt "50"),60c:$(prcnt "50"),70c:$(prcnt "70"),80c:$(prcnt "100"),90c:$(prcnt "100"),100c:$(prcnt "100") -e true -f"

asusctl fan-curve -m performance $main_command gpu
asusctl fan-curve -m performance $main_command cpu

asusctl fan-curve -m balanced $main_command gpu
asusctl fan-curve -m balanced $main_command cpu

asusctl fan-curve -m quiet $main_command gpu
asusctl fan-curve -m quiet $main_command cpu
