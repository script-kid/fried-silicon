#!/bin/bash

# Matches Windows AC Bench

prcnt() {
    echo $((($1*10)*255/1000))
}

main_command="-D 30c:$(prcnt "100"),40c:$(prcnt "100"),50c:$(prcnt "100"),60c:$(prcnt "100"),70c:$(prcnt "100"),80c:$(prcnt "100"),90c:$(prcnt "100"),100c:$(prcnt "100") -e true -f"

asusctl fan-curve -m performance $main_command gpu
asusctl fan-curve -m performance $main_command cpu

asusctl fan-curve -m balanced $main_command gpu
asusctl fan-curve -m balanced $main_command cpu

asusctl fan-curve -m quiet $main_command gpu
asusctl fan-curve -m quiet $main_command cpu
