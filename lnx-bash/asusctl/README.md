# My Zephyrus G14 Linux asusctl fan settings
## fan-curve quiet
asusctl fan-curve -m quiet -D 30c:30,40c:30,50c:40,60c:60,70c:80,80c:100,90c:100,100c:100 -e true -f gpu

asusctl fan-curve -m quiet -D 30c:30,40c:30,50c:40,60c:60,70c:80,80c:100,90c:100,100c:100 -e true -f cpu



## fan-curve balanced (same as quiet for now)
asusctl fan-curve -m balanced -D 30c:30,40c:30,50c:40,60c:60,70c:80,80c:100,90c:100,100c:100 -e true -f gpu

asusctl fan-curve -m balanced -D 30c:30,40c:30,50c:40,60c:60,70c:80,80c:100,90c:100,100c:100 -e true -f cpu



## fan-curve performance
asusctl fan-curve -m performance -D 30c:50,40c:50,50c:50,60c:70,70c:90,80c:100,90c:100,100c:100 -e true -f gpu

asusctl fan-curve -m performance -D 30c:50,40c:50,50c:50,60c:70,70c:90,80c:100,90c:100,100c:100 -e true -f cpu
