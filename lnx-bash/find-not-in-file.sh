#!/bin/bash

# strings to look for
look_for_list=(
	str1
	str2
)

something_found=0
if [ -z "$1" ]; then
	echo "Not set file to look for, use positional param 1"
	exit
fi
for i in "${look_for_list[@]}"; do
	if ! grep -qF "$i" "$1"; then
		# Print a first message to format list
		if [ $something_found = 0 ]; then
			something_found=1
			echo "Items not mentioned in file:"
		fi
		echo " - $i"
	fi
done
# Print only if nothing found for UX reasons
if [ $something_found = 0 ]; then
	echo "All items found"
fi
