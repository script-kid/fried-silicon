#!/bin/bash

# find existing files so start index is correct,
# regex isn't super correct
iteration=0
files=$(find . -name '[0-9]*.*')
counted=$(echo "$files" | wc -l)
echo "counted: $counted"
iteration=$((iteration + counted))
# loop over looking for "unknown.png" (from clipboard)
# or "SPOILER_unknown.png" (Discord spoiler from clipboard)
while :
do
	if test -f "unknown.png" ; then
		mv "unknown.png" "$iteration.png"
		iteration=$((iteration + 1))
		sleep 1
	fi
	elif test -f "SPOILER_unknown.png" ; then
		mv "SPOILER_unknown.png" "$iteration.png"
		iteration=$((iteration + 1))
		sleep 1
	fi
done
