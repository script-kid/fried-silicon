#!/bin/bash

function git_pull_push_timestamped() {
	echo
	echo
	echo "----- Checking Changes -----" 
	ms=$(date +%s%N)
	git pull
	git add .
	git diff --stat --cached
	diffnyan=$(git diff --cached)
	if [ ! -z "$diffnyan" ]
	then
		echo "----- Committing Changes -----" 
		git commit -m "auto-update: $ms"
		git push
	fi
	echo
	echo
}

while true
do 
	git_pull_push_timestamped
	sleep 60
done