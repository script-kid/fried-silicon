#!/usr/bin/tcc -run

#include <stdio.h>

// This is a test file to run C as a "script".
// You need tcc installed - see https://bellard.org/tcc/
//
// To run, chmod+x this file and just call it: `./test.c`

int main() {
	printf("some string\n");
	return 0;
}
